@extends('layouts.app')

@section('page-title', trans(@$title))
@section('page-heading', trans(@$title))

@section('breadcrumbs')
    <li class="breadcrumb-item">
        <a href="/announcements">Announcements</a>
    </li>
    <li class="breadcrumb-item active">
        Edit
    </li>
@stop

@section('content')

@include('partials.messages')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form method="post" action="/announcements/{{ $announcement->id }}" enctype="multipart/form-data">
                    @csrf
                    @method('patch')
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="title">Title*</label>
                            <input id="title" type="text" class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" name="title" placeholder="Title" value="{{ old('title', $announcement->title) }}">
                            <span class="text-danger"><small>{{ $errors->first('title') }}</small></span>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="status">Status*</label>
                            <select id="status" name="status" class="form-control {{ $errors->has('status') ? 'is-invalid' : '' }}">
                                @foreach (\Fteg\Announcement::STATUSES as $status)
                                    <option value="{{ $status }}" {{ old('status', $announcement->status) == $status ? 'selected' : '' }}>{{ $status }}</option>
                                @endforeach
                            </select>
                            <span class="text-danger"><small>{{ $errors->first('status') }}</small></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="image">Image*</label>
                            <input class="form-control {{ $errors->has('image') ? 'is-invalid' : '' }}" name="image" type="file">
                            <img src="{{ $announcement->present()->image }}" class="img-thumbnail" id="preview_img" />
                            <span class="text-danger"><small>{{ $errors->first('image') }}</small></span>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="push_notification">Push Notification*</label>
                            <div class="d-flex align-items-center">
                                <div class="switch">
                                    <input type="hidden" value="0" name="push_notification">
                                    <input type="checkbox" name="push_notification" class="switch" value="1" id="switch-push-notification" {{ $announcement->push_notification == 1 ? 'checked' : '' }}>
                                    <label for="switch-push-notification"></label>
                                </div>
                                <div class="ml-3 d-flex flex-column">
                                    <label class="mb-0">Push Notification</label>
                                    <small class="pt-0 text-muted">
                                        Push Notification to users
                                    </small>
                                </div>
                            </div>
                            <span class="text-danger"><small>{{ $errors->first('push_notification') }}</small></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="content">Content*</label>
                            <textarea id="content" name="content" class="form-control {{ $errors->has('content') ? 'is-invalid' : '' }}" rows=10>{{ old('content', $announcement->content) }}</textarea>
                            <span class="text-danger"><small>{{ $errors->first('content') }}</small></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@stop

@section('scripts')
    {!! wysiwyg_editor_scripts() !!}
    <script type="text/javascript">
        var dom = $('#content');
        var height = 200;
        var url = '/announcements/upload_summernote_image';
        init_summernote(dom, height, url);
    </script>
@stop
