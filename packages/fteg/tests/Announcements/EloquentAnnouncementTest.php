<?php
namespace Fteg\Tests;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;

use Fteg\Announcement;
use Fteg\Announcement\Repositories\EloquentAnnouncement;

class EloquentAnnouncementTest extends TestCase
{
    use RefreshDatabase;

    protected $repo;

    public function setUp() {
        parent::setUp();
        $this->repo = $this->app->make('Fteg\Announcements\Repositories\EloquentAnnouncement');
    }

    public function test_create() {
        $data = [
            'title' => 'New Year Sales',
            'image' => 'test.jpg',
            'push_notification' => 1,
            'content' => '<p>Test</p>',
            'status' => 'Active',
        ];

        $request = request()->add($data);
        $announcement = $this->repo->create($request);

        $this->assertInstanceOf(Announcement::class, $announcement);
        $this->assertEquals($data['title'], $announcement->title);
        $this->assertEquals($data['image'], $announcement->image);
        $this->assertEquals($data['push_notification'], $announcement->push_notification);
        $this->assertEquals($data['content'], $announcement->content);
        $this->assertEquals($data['status'], $announcement->status);
    }
}
