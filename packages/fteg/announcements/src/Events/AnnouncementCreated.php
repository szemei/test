<?php

namespace Fteg\Announcements\Events;

use Fteg\Announcement;
use Fteg\Merchant;

class AnnouncementCreated
{
    public $announcement;

    public function __construct(Announcement $announcement) {
        $this->announcement = $announcement;
    }

}
