<?php

namespace Fteg\Announcements\Observers;

use Vanguard\User;
use Fteg\Announcement;
use Fteg\AppNotification;
use Fteg\Notification\Repositories\EloquentDevice;

class AnnouncementObserver
{
    public function created(Announcement $announcement)
    {

    }

    public function updated(Announcement $announcement)
    {

    }

    public function deleted(Announcement $announcement)
    {

    }

    public function saved(Announcement $announcement) {

    }
}
