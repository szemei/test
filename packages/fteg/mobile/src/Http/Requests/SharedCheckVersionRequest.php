<?php
namespace Fteg\Http\Requests\Mobile;

use Vanguard\Http\Requests\FormRequest;
use Validator;

class SharedCheckVersionRequest extends FormRequest
{
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'app_version' => 'required',
            'app_type' => 'required',
            'bundle_ios' => 'required_if:bundle_android,""',
            'bundle_android' => 'required_if:bundle_ios,""',
        ];
    }

    public function messages() {
        return [
            'bundle_ios.required_if' => 'The :attribute field or',
            'bundle_android.required_if' => ':attribute field is required.',
        ];
    }
}
