<?php
namespace Fteg\Http\Requests\Mobile;

use Vanguard\Http\Requests\FormRequest;
use Validator;

class RegisterDeviceTokenRequest extends FormRequest
{
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'merchant_id'   => 'sometimes|exists:MerchantMaster,id',
            'uuid'          => 'required',
            'brand'         => 'required',
            'modal'         => 'required',
            'os'            => 'required',
            'os_version'    => 'required',
            'device_token'  => 'required',
            'app_version'   => 'required',
        ];
    }

    public function messages() {
        return [
            'bundle_ios.required_if' => 'The :attribute field or',
            'bundle_android.required_if' => ':attribute field is required.',
        ];
    }
}
