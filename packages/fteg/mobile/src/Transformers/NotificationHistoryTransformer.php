<?php
namespace Fteg\Mobile\Transformers;

use League\Fractal\TransformerAbstract;
use Fteg\NotificationHistory;

class NotificationHistoryTransformer extends TransformerAbstract
{
    public function transform(NotificationHistory $notification_history) {
        return [
            'id' => $notification_history->id,
            'title' => $notification_history->title,
            'message' => $notification_history->message,
            'action' => $notification_history->action,
            'data' => json_decode($notification_history->data),
            'read' => $notification_history->read,
            'created_at' => $notification_history->created_at->format('Y-m-d H:i:s'),
        ];
    }
}
