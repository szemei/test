<?php
namespace Fteg\Mobile\Repositories;

use Schema;
use Fteg\Device;
use DB;
use JWTAuth;
use Illuminate\Http\Request;
use Fteg\Merchant;

use Vanguard\User;

class EloquentDevice
{

    public function query() {
        return Device::join('users', 'users.id', '=', 'DeviceMaster.user_id')
        ->join('MemberMaster', 'MemberMaster.user_id', '=', 'users.id')
        ->where('DeviceMaster.in_use', 1)
        ->select('DeviceMaster.*', 'users.id as user_id', 'users.phone', 'users.username');
    }

    public function find($id) {
        return $this->query()->where('DeviceMaster.id', $id)->first();
    }

    public function find_by_token($token) {
        return $this->query()->where('DeviceMaster.token', $token)->first();
    }

    public function find_by_user_id($user_id) {
        return $this->query()->where('DeviceMaster.user_id', $user_id)->get();
    }

    public function createOrUpdate(Request $request, User $user, $token = null) {
        $data = $request->all();
        $data['user_id'] = $user->id;
        $data['in_use'] = 1;
        $query = Device::where('uuid', $request->uuid)->where('user_id', $user->id);

        if ($request->has('merchant_id'))
        $query->where('merchant_id', $request->merchant_id);

        $device = $query->first();

        if ($device) {
            $data = [
                'device_token' => $request->device_token,
                'app_version' => $request->app_version,
                'in_use' => 1
            ];

            if ($token != null)
                $data['token'] = $token;
            $device->update($data);
        } else {
            $user->devices()->create($request->all());
        }

        // update current device to be in use
        Device::where('uuid', $request->uuid)
        ->where('user_id', $user->id)
        ->update(['in_use' => true]);

        $query = Device::where('uuid', '!=', $request->uuid)->where('user_id', $user->id);
        if ($request->has('merchant_id'))
            $query->where('merchant_id', $request->merchant_id);
        else
            $query->where('merchant_id', null);

        $other_devices = $query->get();
        Device::whereIn('id', $other_devices->pluck('id')->toArray())->update(['in_use' => false]);

        // for single login, disable for multi login
        // foreach($other_devices as $device) {
        //     if ($device->token != null && $device->merchant_id == null) {
        //         if (JWTAuth::setToken($device->token)->check())
        //             JWTAuth::setToken($device->token)->invalidate();
        //     }
        // }
    }

    public function check_version(Request $request, Merchant $merchant = null) {
        $bundle_is_correct = '';
        $download_link = '';

        // if merchant is null, means shared-app version checking
        // else white-label app version checking
        if (!$merchant) {
            $bundle_is_correct = $this->is_correct_shared_bundle($request);
            $download_link = $this->get_shared_download_link($request);
            $is_latest = $this->is_latest_shared_version($request);
        } else {
            $bundle_is_correct = $this->is_correct_white_label_bundle($request, $merchant);
            $download_link = $this->get_white_label_download_link($request, $merchant);
            $is_latest = $this->is_latest_white_label_version($request, $merchant);
        }

        if (!$bundle_is_correct) {
            return [
                'status' => 0,
                'msg' => 'Sorry, this application has no longer provide support. Kindly download the new version.',
                'app_eliminate' => 1,
                'download' => $download_link
            ];
        }

        if (!$is_latest)
        return ['status' => 0, 'msg' => 'New version found.', 'download' => $download_link, 'force_update' => 1, 'update' => 1];

        return ['status' => 1, 'msg' => 'Valid', 'download' => '', 'force_update' => 0, 'update' => 0];
    }

    protected function is_latest_shared_version(Request $request) {
        $current_version = '';

        if ($request->has('bundle_android') && $request->app_type == Device::APP_TYPE_MEMBER)
        $current_version = env('ANDROID_VERSION');

        if ($request->has('bundle_android') && $request->app_type == Device::APP_TYPE_MERCHANT)
        $current_version = env('ANDROID_MERCHANT_VERSION');

        if ($request->has('bundle_ios') && $request->app_type == Device::APP_TYPE_MEMBER)
        $current_version = env('IOS_VERSION');

        if ($request->has('bundle_ios') && $request->app_type == Device::APP_TYPE_MERCHANT)
        $current_version = env('IOS_MERCHANT_VERSION');

        switch (version_compare($request->app_version, $current_version)) {
            // same version
            case 0:
                return true;
            // app version higher than current version
            case 1:
                return true;
            // outdated
            case -1:
                return false;
        }
    }

    protected function is_latest_white_label_version(Request $request, Merchant $merchant) {
        $current_version = '';

        if ($request->has('bundle_android') && $request->app_type == Device::APP_TYPE_MEMBER)
        $current_version = $merchant->android_member_app_version;

        if ($request->has('bundle_android') && $request->app_type == Device::APP_TYPE_MERCHANT)
        $current_version = $merchant->android_merchant_app_version;

        if ($request->has('bundle_ios') && $request->app_type == Device::APP_TYPE_MEMBER)
        $current_version = $merchant->ios_member_app_version;

        if ($request->has('bundle_ios') && $request->app_type == Device::APP_TYPE_MERCHANT)
        $current_version = $merchant->ios_merchant_app_version;

        $version = explode('.', $current_version);
        $app_version = explode('.', $request->app_version);

        foreach($version as $key => $ver){
            if($ver > @$app_version[$key]){
                return false;
            }
        }
        return true;
    }

    protected function get_shared_download_link(Request $request) {
        if ($request->has('bundle_android') && $request->app_type == Device::APP_TYPE_MEMBER)
        return env('ANDROID_DOWNLOAD');

        if ($request->has('bundle_android') && $request->app_type == Device::APP_TYPE_MERCHANT)
        return env('ANDROID_MERCHANT_DOWNLOAD');

        if ($request->has('bundle_ios') && $request->app_type == Device::APP_TYPE_MEMBER)
        return env('IOS_DOWNLOAD');

        if ($request->has('bundle_ios') && $request->app_type == Device::APP_TYPE_MERCHANT)
        return env('IOS_MERCHANT_DOWNLOAD');

        return '';
    }

    protected function get_white_label_download_link(Request $request, Merchant $merchant) {
        if ($request->has('bundle_android') && $request->app_type == Device::APP_TYPE_MEMBER)
        return $merchant->android_member_download_link;

        if ($request->has('bundle_android') && $request->app_type == Device::APP_TYPE_MERCHANT)
        return $merchant->android_merchant_download_link;

        if ($request->has('bundle_ios') && $request->app_type == Device::APP_TYPE_MEMBER)
        return $merchant->ios_member_download_link;

        if ($request->has('bundle_ios') && $request->app_type == Device::APP_TYPE_MERCHANT)
        return $merchant->ios_merchant_download_link;

        return '';
    }

    protected function is_correct_shared_bundle(Request $request) {
        if ($request->has('bundle_android') && $request->app_type == Device::APP_TYPE_MEMBER)
        return $request->bundle_android == env('BUNDLE_ANDROID');

        if ($request->has('bundle_android') && $request->app_type == Device::APP_TYPE_MERCHANT)
        return $request->bundle_android == env('BUNDLE_MERCHANT_ANDROID');

        if ($request->has('bundle_ios') && $request->app_type == Device::APP_TYPE_MEMBER)
        return $request->bundle_ios == env('BUNDLE_IOS');

        if ($request->has('bundle_ios') && $request->app_type == Device::APP_TYPE_MERCHANT)
        return $request->bundle_ios == env('BUNDLE_MERCHANT_IOS');

        return false;
    }

    protected function is_correct_white_label_bundle(Request $request, Merchant $merchant) {
        if ($request->has('bundle_android') && $request->app_type == Device::APP_TYPE_MEMBER)
        return $request->bundle_android == $merchant->android_member_app_id;

        if ($request->has('bundle_android') && $request->app_type == Device::APP_TYPE_MERCHANT)
        return $request->bundle_android == $merchant->android_merchant_app_id;

        if ($request->has('bundle_ios') && $request->app_type == Device::APP_TYPE_MEMBER)
        return $request->bundle_ios == $merchant->ios_member_app_id;

        if ($request->has('bundle_ios') && $request->app_type == Device::APP_TYPE_MERCHANT)
        return $request->bundle_ios == $merchant->ios_merchant_app_id;

        return false;
    }

    public function logout($user_id) {
        Device::where('user_id', $user_id)->where('in_use', 1)
        ->update(['in_use' => 0, 'token' => '']);
    }
}
