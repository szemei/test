<?php
namespace Fteg\Announcements\Listeners;

use Fteg\Announcements\Events\AnnouncementCreated;
use Fteg\Announcements\Events\AnnouncementUpdated;
use Fteg\Announcement;

use Fteg\Users\Repositories\EloquentUser;

class AnnouncementEventSubscriber
{
    public function onAnnouncementCreated(AnnouncementCreated $event) {
        // send notification
    }

    public function onAnnouncementUpdated(AnnouncementUpdated $event) {
        // send notification
    }

    public function subscribe($events) {
        $events->listen(
            AnnouncementCreated::class,
            'Fteg\Announcements\Listeners\AnnouncementEventSubscriber@onAnnouncementCreated'
        );

        $events->listen(
            AnnouncementUpdated::class,
            'Fteg\Announcements\Listeners\AnnouncementEventSubscriber@onAnnouncementUpdated'
        );
    }
}
