<?php
namespace Fteg\Mobile\Controllers;

use Vanguard\Http\Controllers\Controller;

use DB;
use Fteg\Merchant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Fteg\Mobile\Repositories\EloquentDevice;

use Fteg\Http\Requests\Mobile\SharedCheckVersionRequest;
use Fteg\Http\Requests\Mobile\WhiteLabelCheckVersionRequest;
use Fteg\Http\Requests\Mobile\RegisterDeviceTokenRequest;

use Fteg\AppNotification;

class ApiMobileController extends Controller
{
    protected $deviceRepo;

    protected function register_middleware() {
        $this->middleware(['api', 'maintenance']);
        $this->middleware(['receive', 'response']);
        $this->middleware('signature');
        $this->middleware('jwt', ['only' => ['update_device_info', 'notification_list', 'notification_read']]);
    }

    public function __construct(EloquentDevice $deviceRepo) {
        $this->register_middleware();
        $this->deviceRepo = $deviceRepo;
    }

    public function create_table() {
        $this->deviceRepo->setUp();
    }

    public function update_device_info(RegisterDeviceTokenRequest $request) {
        $user = auth()->user();
        $token = $request->bearerToken();
        $this->deviceRepo->createOrUpdate($request, $user, $token);
        return ['status' => 1, 'msg' => 'Valid'];
    }

    public function verify_shared_app(SharedCheckVersionRequest $request) {
        return $this->deviceRepo->check_version($request);
    }

    public function verify_white_label_app(WhiteLabelCheckVersionRequest $request) {
        $merchant = Merchant::find($request->merchant_id);
        if ($merchant->level != Merchant::LEVEL_WHITE_LABEL)
            return ['status' => 0, 'msg' => 'This app is no longer usable. Please proceed to download our Beelongz App to continue using.', 'maintenance' => 1];

        return $this->deviceRepo->check_version($request, $merchant);
    }

    public function checkMaintenance() {
        if (env('MAINTENANCE')) {
            return [
                'status' => 0,
                'msg' => 'Sorry for inconvenience, we currently under maintenance, please try again later.',
                'maintenance' => 1,
                'logout' => 1
            ];
        }

        return ['status' => 1, 'msg' => 'Valid.'];
    }
}
