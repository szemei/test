<?php

use Vanguard\Permission;
use Vanguard\Role;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = Role::where('name', 'Admin')->first();

        $permissions[] = Permission::create([
            'name' => 'users.manage',
            'display_name' => 'Manage Users',
            'description' => 'Manage users and their sessions.',
            'removable' => false
        ]);

        $permissions[] = Permission::create([
            'name' => 'users.activity',
            'display_name' => 'View System Activity Log',
            'description' => 'View activity log for all system users.',
            'removable' => false
        ]);

        $permissions[] = Permission::create([
            'name' => 'roles.manage',
            'display_name' => 'Manage Roles',
            'description' => 'Manage system roles.',
            'removable' => false
        ]);

        $permissions[] = Permission::create([
            'name' => 'permissions.manage',
            'display_name' => 'Manage Permissions',
            'description' => 'Manage role permissions.',
            'removable' => false
        ]);

        $permissions[] = Permission::create([
            'name' => 'settings.general',
            'display_name' => 'Update General System Settings',
            'description' => '',
            'removable' => false
        ]);

        $permissions[] = Permission::create([
            'name' => 'settings.auth',
            'display_name' => 'Update Authentication Settings',
            'description' => 'Update authentication and registration system settings.',
            'removable' => false
        ]);

        $permissions[] = Permission::create([
            'name' => 'settings.notifications',
            'display_name' => 'Update Notifications Settings',
            'description' => '',
            'removable' => false
        ]);

        $adminRole->attachPermissions($permissions);

        $other_permissions = [
            // ['name' => 'view.merchants', 'display_name' => 'View Merchants', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'create.merchants', 'display_name' => 'Create Merchants', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'edit.merchants', 'display_name' => 'Edit Merchants', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'delete.merchants', 'display_name' => 'Delete Merchants', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'edit.merchant.information', 'display_name' => 'Edit Merchant Information', 'description' => "Edit merchant's about us, contact us, branches, tiers", 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'search.users', 'display_name' => 'Search Users', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'view.members', 'display_name' => 'View Members', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'create.members', 'display_name' => 'Create Members', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'edit.members', 'display_name' => 'Edit Members', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'block.members', 'display_name' => 'Block Members', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'view.subscribers', 'display_name' => 'create.subscribers', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'create.subscribers', 'display_name' => 'Create Subscribers', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'edit.subscribers', 'display_name' => 'Edit Subscribers', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'delete.subscribers', 'display_name' => 'Delete Subscribers', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'view.staffs', 'display_name' => 'View Staffs', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'create.staffs', 'display_name' => 'Create Staffs', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'edit.staffs', 'display_name' => 'Edit Staffs', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'delete.staffs', 'display_name' => 'Delete Staffs', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'view.announcements', 'display_name' => 'View Announcements', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'create.announcements', 'display_name' => 'Create Announcements', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'edit.announcements', 'display_name' => 'Edit Announcements', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'delete.announcements', 'display_name' => 'Delete Announcements', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'view.packages', 'display_name' => 'View Packages', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'create.packages', 'display_name' => 'Create Packages', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'edit.packages', 'display_name' => 'Edit Packages', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'delete.packages', 'display_name' => 'Delete Packages', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'cancel.packages', 'display_name' => 'Cancel Packages', 'description' => 'Cancel packages of existing members.', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'redeem.services', 'display_name' => 'Redeem Services', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'view.services', 'display_name' => 'View Services', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'create.services', 'display_name' => 'Create Services', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'edit.services', 'display_name' => 'Edit Services', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'delete.services', 'display_name' => 'Delete Services', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'view.orders', 'display_name' => 'View Orders', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'create.orders', 'display_name' => 'Create Orders', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'edit.orders', 'display_name' => 'Edit Orders', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'cancel.orders', 'display_name' => 'Cancel Orders', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'create.bookings', 'display_name' => 'Create Bookings', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'view.bookings', 'display_name' => 'View Bookings', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'edit.bookings', 'display_name' => 'Edit Bookings', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            // ['name' => 'delete.bookings', 'display_name' => 'Delete Bookings', 'description' => '', 'removable' => 'true', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
        ];

        Permission::insert($other_permissions);
    }
}
