<?php
Route::namespace('Fteg\Announcements\Controllers')->group(function() {
	if (@$_SERVER['HTTP_HOST'] == env('ADMIN_DOMAIN')) {
		Route::post('announcements/upload_summernote_image', 'AdminAnnouncementController@uploadImage');
		Route::resource('announcements', 'AdminAnnouncementController')->except(['show']);
	}
});
