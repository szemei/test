<?php

Route::namespace('Fteg\Mobile\Controllers')->prefix('api/v1')->group(function() {
	Route::post('devices', 'ApiMobileController@update_device_info');
	
	if (@$_SERVER['HTTP_HOST'] == env('API_DOMAIN')) {
        Route::post('check-version/shared', 'ApiMobileController@verify_shared_app');
        Route::post('check-version/white-label', 'ApiMobileController@verify_white_label_app');

		Route::get('notifications', 'ApiNotificationController@user_list');
		Route::patch('notifications/{notification}/read', 'ApiNotificationController@read');
	}

	if (@$_SERVER['HTTP_HOST'] == env('MERCHANT_API_DOMAIN')) {
		Route::post('check-version/shared', 'ApiMobileController@verify_shared_app');
        Route::post('check-version/white-label', 'ApiMobileController@verify_white_label_app');

		Route::get('notifications', 'ApiNotificationController@merchant_list');
		Route::patch('notifications/{notification}/read', 'ApiNotificationController@read');
	}
	//
	// if (@$_SERVER['HTTP_HOST'] == env('MERCHANT_API_DOMAIN')) {
	// 	Route::prefix('api')->group(function() {
	// 		Route::post('check/version', 'ApiMobileController@verifyMerchantAppVersion');
	// 	});
	// }
});
