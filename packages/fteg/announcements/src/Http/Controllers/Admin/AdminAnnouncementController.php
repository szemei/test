<?php

namespace Fteg\Announcements\Controllers;

use Vanguard\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Fteg\Announcements\Repositories\EloquentAnnouncement;
use Fteg\Http\Requests\Announcements\CreateAnnouncementRequest;
use Fteg\Http\Requests\Announcements\UpdateAnnouncementRequest;

use Fteg\Announcements\Events\AnnouncementCreated;
use Fteg\Announcements\Events\AnnouncementUpdated;

use View;
use Fteg\Announcement;
use Carbon\Carbon;

class AdminAnnouncementController extends Controller
{
    protected $announcementRepo;

    protected function register_middleware() {
        $this->middleware(['web', 'auth']);
        $this->middleware('admin_access_only');
    }

    public function __construct(EloquentAnnouncement $announcementRepo) {
        $this->register_middleware();
        $this->announcementRepo = $announcementRepo;

        $title = 'Announcement';
        View::share(compact('title'));
    }

    public function index(Request $request) {
        $announcements = $this->announcementRepo->list(true, $request);
        return view('announcement::admin.index', compact('announcements'));
    }

    public function create() {
        return view('announcement::admin.create');
    }

    public function store(CreateAnnouncementRequest $request) {
        $announcement = $this->announcementRepo->create($request);
        event(new AnnouncementCreated($announcement));
        return redirect('announcements')->with('success', 'Announcement created.');
    }

    public function edit(Announcement $announcement) {
        return view('announcement::admin.edit', compact('announcement'));
    }

    public function update(UpdateAnnouncementRequest $request, Announcement $announcement) {
        $this->announcementRepo->update($announcement);
        event(new AnnouncementUpdated($announcement->fresh()));
        return redirect('announcements')->with('success', 'Announcement updated.');
    }

    public function destroy(Request $request, Announcement $announcement) {
        $status = $this->announcementRepo->delete($announcement);

        return redirect('announcements')->with('success', 'Deleted.');
    }

    public function uploadImage() {
        $file = request()->file('file');
        return upload_image_with_json_response($file, Announcement::UPLOAD_PATH);
    }
}
