<?php

namespace Fteg;

use Illuminate\Database\Eloquent\Model;

class NotificationHistory extends Model
{
    protected $table = 'NotificationMaster';
    protected $fillable = ['user_id', 'merchant_id', 'type', 'module', 'module_id', 'action', 'title',
                            'target', 'multicast_id', 'status', 'canonical_ids', 'result',
                            'message', 'read', 'created_at', 'updated_at'];

    const TARGET_MEMBER = 'member';
    const TARGET_MERCHANT = 'merchant';

    const TYPE_GENERAL = 'general';
    const TYPE_MERCHANT = 'merchant';

    const STATUS_FAIL = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_REMOVED = 2;

    public function hasBeenRead() {
        $this->update(['read' => true]);
    }
}
