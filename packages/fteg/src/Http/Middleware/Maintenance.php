<?php

namespace Fteg\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Carbon\Carbon;

class Maintenance
{
    protected $auth;
    /**
     * Creates a new instance of the middleware.
     *
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(env('MAINTENANCE')){
            $result['status'] = 0;
            $result['msg'] = 'Sorry for inconvenience, we currently under maintenance, please try again later.';
            $result['maintenance'] = 1;
            $result['action'] = 'logout';
            return response($result);
        }
        return $next($request);
    }

}
