<?php
namespace Fteg;

use Artisan;
use Auth;
use Illuminate\Support\ServiceProvider;

class FtegServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot() {
        if(env('APP_ENV') == 'development'){
            Artisan::call('view:clear');
        }
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register() {
        $this->app->make('Fteg\Http\Kernel');
    }
}
