<?php
Route::namespace('Fteg\Announcements\Controllers')->prefix('api/v1')->group(function() {
    /******************** USER ********************/
    if (@$_SERVER['HTTP_HOST'] == env('API_DOMAIN')) {
        Route::get('announcements', 'ApiUserAnnouncementController@index');
        Route::get('announcements/{announcement}/show', 'ApiUserAnnouncementController@show');
    }

    if (@$_SERVER['HTTP_HOST'] == env('MERCHANT_API_DOMAIN')) {
        Route::get('announcements', 'ApiMerchantAnnouncementController@index');
        Route::get('announcements/{announcement}/show', 'ApiMerchantAnnouncementController@show');
        Route::patch('announcements/{announcement}', 'ApiMerchantAnnouncementController@update');
        Route::delete('announcements/{announcement}', 'ApiMerchantAnnouncementController@destroy');
    }

});
