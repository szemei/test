<?php

namespace Fteg\PublicSite;

use Illuminate\Support\ServiceProvider;

class FtegPublicSiteServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/views', 'public_site');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/routes/web.php';

        $controllers_array = ['\PublicSite'];
        foreach($controllers_array as $controller){
            $this->app->make('Fteg\PublicSite\Controllers'.$controller.'Controller');
        }
        // $this->loadViewsFrom(__DIR__.'/views', 'company');
        // $this->loadRoutesFrom(__DIR__.'/routes/web.php');
    }
}
