<?php
namespace Vanguard\Console\Commands;

use Fteg\Member;
use Fteg\PackagesOwned;
use Fteg\AppNotification;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UpdatePackageStatus extends Command
{
    protected $signature = 'fteg:update_package_status';
    protected $description = 'Update member\'s package status when expire.';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        $this->update_package_status();
    }

    protected function update_package_status() {
        $today = Carbon::today();
        PackagesOwned::where('expiry_date', '<', $today)->update(['status' => PackagesOwned::STATUS_EXPIRED]);
    }
}
