$(document).ready(function() {

});


function init_summernote(dom, height, url) {
    dom.summernote({
        height: height,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['Insert', ['picture', 'link', 'table', 'hr']],
            ['Other', ['fullscreen', 'codeview', 'undo', 'redo']]
        ],
        callbacks: {
            onInit: function() {
                var code = $(this).summernote('code');
                code = code.replace(/&lt;/g,'<');
                code = code.replace(/&gt;/g,'>');
                code = code.replace(/&quot;/g,'"');
                code = code.replace(/&amp;/g,'&');
                code = code.replace(/&#039;/g,'\'');
                $(this).summernote('code',code);
            },
            onImageUpload: function(files) {
                uploadFiles(this, files, url);
            },
        }
    });
}

function uploadFiles(summerNoteObj, files, url){
    data = new FormData();
    data.append("file", files[0]);
    $.ajax({
        url: url,
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        type: 'POST',
        success: function(res){
            if (res.status == 'success') {
                var imgNode = document.createElement('img');
                imgNode.src = res.file_path;
                $(summerNoteObj).summernote('insertNode', imgNode);
            } else {
                alert(res.message);
            }

            console.log(res);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus+" "+errorThrown);
        }
    });
}

$("input[type='file']").on('change', function() {
    for (var i = 0; i < this.files.length; i++) {
        if (this.files[i].size > 1024000) {
            alert("Maximum file size allowed is 1mb.");
            this.value = "";
        }
    }
});


$('.select-filter').on('change', function() {
    
    // alert(this.name)
})
// var uploadField = document.getElementById("file");
//
// uploadField.onchange = function() {
//     if(this.files[0].size > 2097152){
//        alert("File is too big!");
//        this.value = "";
//     };
// };
