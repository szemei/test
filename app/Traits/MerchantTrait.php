<?php
namespace Vanguard\Traits;

use Illuminate\Http\Request;
use Vanguard\User;
use Fteg\Merchant;

trait MerchantTrait {
    // public function merchant($user_id) {
    //     return $this->belongsTo(Merchant::class)->where('merchant_user.user_id', $user_id);
    // }
    // public function user_info() {
    //     return $this->hasOne(UserInfo::class);
    // }
    //
    // public function set_verified() {
    //     return $this->user_info()->update(['verified' => true]);
    // }

    public function merchants() {
        return $this->belongsToMany(Merchant::class);
    }

    public function has_merchant(Merchant $merchant) {
        return $this->merchants()->contains($merchant);
    }
}
