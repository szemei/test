<?php

namespace Fteg\Mobile;

use Illuminate\Support\ServiceProvider;

class MobileServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/views', 'mobile');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/routes/web.php';
        include __DIR__.'/routes/v1/api.php';
    }
}
